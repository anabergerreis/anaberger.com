const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: './app.js',
  output: {
    filename: 'app.bundle.js',
    path: `${__dirname}/public`
  },
  module: {
    rules: [
      {
        test: /\.njk$/,
        use: {
          loader: 'nunjucks-render-loader',
          options: {
            path: `${__dirname}/src/views`
          }
        }
      }
    ]
  },
  devServer: {
    contentBase: `${__dirname}/public`,
    port: 3000
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/views/index.njk'
    })
  ]
}