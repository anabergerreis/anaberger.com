const express = require('express');
const path = require('path');

const app = express();

app.use(express.static('public'));

//using template engine
const nunjucks = require("nunjucks")//requesting a module that has already been installed
nunjucks.configure("src/views",{ //what folder are the html(1argument), objects(2argument)
	express: app,
	noCache: true //do not return old version html
}) 


const PORT = 3000;



app.get('/', (req, res) => {
	 return res.render('index.html')
})

app.listen(PORT, () => console.log(`servidor rodando em localhost:${PORT}`))


