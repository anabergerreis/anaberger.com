<h1 align="center">anaberger.com</h1>

<img src="https://badgen.net/badge/npm/6.14.4/pink">
<img src="https://badgen.net/badge/nunjucks/3.2.1/pink">
<img src="https://badgen.net/badge/node/12.17.0/pink">
<img src="https://badgen.net/badge/webpack/4.43.0/pink">


<h3>Objetivo:</h3>
<p>A criação do meu portfólio teve como intuito aprimorar alguns dos meus conhecimentos e conseguir mostrar um pouco do meu trabalho </p>

##
<h3>Scripts:</h3>
Gerar bundle: 
<code>npm run build</code>
<br/>
Iniciar ambiente de desenvolvimento
<code>npm start</code>
