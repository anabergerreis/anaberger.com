/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "public/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("//função efeito home\n\n(function particles() {\n  particlesJS(\"particles-js\", {\n    \"particles\": {\n      \"number\": {\n        \"value\": 161,\n        \"density\": {\n          \"enable\": true,\n          \"value_area\": 800\n        }\n      },\n      \"color\": {\n        \"value\": \"#ffffff\"\n      },\n      \"shape\": {\n        \"type\": \"circle\",\n        \"stroke\": {\n          \"width\": 0,\n          \"color\": \"#000000\"\n        },\n        \"polygon\": {\n          \"nb_sides\": 5\n        },\n        \"image\": {\n          \"src\": \"img/github.svg\",\n          \"width\": 100,\n          \"height\": 100\n        }\n      },\n      \"opacity\": {\n        \"value\": 0.5,\n        \"random\": false,\n        \"anim\": {\n          \"enable\": false,\n          \"speed\": 1,\n          \"opacity_min\": 0.1,\n          \"sync\": false\n        }\n      },\n      \"size\": {\n        \"value\": 3,\n        \"random\": true,\n        \"anim\": {\n          \"enable\": false,\n          \"speed\": 40,\n          \"size_min\": 0.1,\n          \"sync\": false\n        }\n      },\n      \"line_linked\": {\n        \"enable\": false,\n        \"distance\": 150,\n        \"color\": \"#ffffff\",\n        \"opacity\": 0.4,\n        \"width\": 1\n      },\n      \"move\": {\n        \"enable\": true,\n        \"speed\": 3,\n        \"direction\": \"none\",\n        \"random\": true,\n        \"straight\": false,\n        \"out_mode\": \"out\",\n        \"bounce\": false,\n        \"attract\": {\n          \"enable\": false,\n          \"rotateX\": 163.9787811457196,\n          \"rotateY\": 491.9363434371588\n        }\n      }\n    },\n    \"interactivity\": {\n      \"detect_on\": \"canvas\",\n      \"events\": {\n        \"onhover\": {\n          \"enable\": false,\n          \"mode\": \"repulse\"\n        },\n        \"onclick\": {\n          \"enable\": false,\n          \"mode\": \"push\"\n        },\n        \"resize\": true\n      },\n      \"modes\": {\n        \"grab\": {\n          \"distance\": 400,\n          \"line_linked\": {\n            \"opacity\": 1\n          }\n        },\n        \"bubble\": {\n          \"distance\": 400,\n          \"size\": 40,\n          \"duration\": 2,\n          \"opacity\": 8,\n          \"speed\": 3\n        },\n        \"repulse\": {\n          \"distance\": 200,\n          \"duration\": 0.4\n        },\n        \"push\": {\n          \"particles_nb\": 4\n        },\n        \"remove\": {\n          \"particles_nb\": 2\n        }\n      }\n    },\n    \"retina_detect\": true\n  });\n}());\n\n\n//função efeito do menu\n\n(function init() {\n  const menuAnimation = () => {\n    // const { scrollTop } = document.documentElement;\n    const scrollTop = document.documentElement.scrollTop;\n    const menu = document.querySelector('nav');\n    const header = document.querySelector('header');\n    const wave = document.getElementById('header-wave');\n    const buttonRocket = getRocketButton();\n\n    // const { clientHeight: headerHeight } = header;\n    const headerHeight = header.clientHeight;\n    const waveHeight = wave.clientHeight;\n    \n    if (scrollTop > headerHeight + waveHeight) {\n      menu.classList.add('active');\n      buttonRocket.classList.add('active');\n    } else {\n      menu.classList.remove('active');\n      buttonRocket.classList.remove('active');\n    }\n  };\n\n  const scrollAnimations = event => {\n    const { scrollY } = event.target.defaultView;\n    const animatedElement = document.querySelectorAll('[data-aos=\"true\"]');\n    const { innerHeight: windowHeight } = window;\n\n    animatedElement.forEach(element => {\n      if (scrollY + windowHeight > element.offsetTop + 150) {\n        element.classList.remove('waiting');\n      }\n    })\n  }\n\n  document.addEventListener('scroll', scrollAnimations);\n  document.addEventListener('scroll', menuAnimation);\n}());\n\nif (window.innerWidth < 768) {\n  (function menuLinks() {\n    document.querySelectorAll('li')\n    .forEach(listElement => {\n      listElement.onclick = () => {\n        const navElement = document.querySelector('#navbarSupportedContent');\n        navElement.classList.toggle('show');\n      }\n    })\n  }());\n}\n\nfunction getRocketButton() {\n  const button = document.querySelector('#button-rocket');\n\n  return button;\n}\n\n\n\n//função da seção de contato\n\nfunction main() {\n  const text = document.querySelector('#contact p');\n  const spaceship = document.getElementById('spaceship');\n \n  button.onclick = function () {\n    text.classList.toggle('active');\n    spaceship.classList.toggle('active');\n  }\n}\n\nmain();\n\nfunction buttonRocket () {\n  const buttonRocket = getRocketButton();\n    buttonRocket.onclick = function () {\n      document.body.scrollTop = 0;\n      document.documentElement.scrollTop = 0;\n    }\n}\n\nbuttonRocket();\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });