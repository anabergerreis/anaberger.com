
//função efeito home

(function particles() {
  particlesJS("particles-js", {
    "particles": {
      "number": {
        "value": 161,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#ffffff"
      },
      "shape": {
        "type": "circle",
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.5,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 3,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": false,
        "distance": 150,
        "color": "#ffffff",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 3,
        "direction": "none",
        "random": true,
        "straight": false,
        "out_mode": "out",
        "bounce": false,
        "attract": {
          "enable": false,
          "rotateX": 163.9787811457196,
          "rotateY": 491.9363434371588
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": false,
          "mode": "repulse"
        },
        "onclick": {
          "enable": false,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200,
          "duration": 0.4
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true
  });
}());


//função efeito do menu

(function init() {
  const menuAnimation = () => {
    // const { scrollTop } = document.documentElement;
    const scrollTop = document.documentElement.scrollTop;
    const menu = document.querySelector('nav');
    const header = document.querySelector('header');
    const wave = document.getElementById('header-wave');
    const buttonRocket = getRocketButton();

    // const { clientHeight: headerHeight } = header;
    const headerHeight = header.clientHeight;
    const waveHeight = wave.clientHeight;
    
    if (scrollTop > headerHeight + waveHeight) {
      menu.classList.add('active');
      buttonRocket.classList.add('active');
    } else {
      menu.classList.remove('active');
      buttonRocket.classList.remove('active');
    }
  };

  const scrollAnimations = event => {
    const { scrollY } = event.target.defaultView;
    const animatedElement = document.querySelectorAll('[data-aos="true"]');
    const { innerHeight: windowHeight } = window;

    animatedElement.forEach(element => {
      if (scrollY + windowHeight > element.offsetTop + 150) {
        element.classList.remove('waiting');
      }
    })
  }

  document.addEventListener('scroll', scrollAnimations);
  document.addEventListener('scroll', menuAnimation);
}());

if (window.innerWidth < 768) {
  (function menuLinks() {
    document.querySelectorAll('li')
    .forEach(listElement => {
      listElement.onclick = () => {
        const navElement = document.querySelector('#navbarSupportedContent');
        navElement.classList.toggle('show');
      }
    })
  }());
}

function getRocketButton() {
  const button = document.querySelector('#button-rocket');

  return button;
}



//função da seção de contato

function main() {
  const text = document.querySelector('#contact p');
  const spaceship = document.getElementById('spaceship');
 
  button.onclick = function () {
    text.classList.toggle('active');
    spaceship.classList.toggle('active');
  }
}

main();

function buttonRocket () {
  const buttonRocket = getRocketButton();
    buttonRocket.onclick = function () {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }
}

buttonRocket();
